Họ tên: Trần Thị Nhã
MSSV: 1512364
Email: trannha132@gmail.com

1. Các chức năng làm được:
Người dùng khởi chạy ứng dụng. Một biểu tượng sẽ được thêm vào khu vực thông báo 
+ Người dùng có thể nhấp chuột phải vào nó để xem menu: Xem ghi chú, Xem statitistics, Thoát.
- Thoát menu, tất nhiên, sẽ thoát khỏi ứng dụng.
- Xem ghi chú: Một danh sách xem sẽ hiển thị tất cả các thẻ, mỗi thẻ có số lượng các ghi chú tương ứng với nó. 
Khi nhấp vào một thẻ, một danh sách tất cả các ghi chú sẽ được hiển thị một thời gian ngắn  trong listview. 
Khi nhấp vào một mục trên listview, nội dung đầy đủ của ghi chú sẽ xuất hiện trên hộp thoại hoặc hộp văn bản. 
- Xem số liệu thống kê: Hiển thị biểu đồ dựa trên thẻ và số ghi chú tương ứng. 

2. Các chức năng chưa làm được:
- Người dùng nhấn phím tắt, một hộp thoại sẽ xuất hiện cho người dùng gõ ghi chú, nhập nhiều thẻ, phân cách bằng dấu phẩy (",") rồi nhấp vào Lưu. 

2. Link Youtube: https://youtu.be/-1BtZ8t1Ieo

3. Link Bitbucket: https://bitbucket.org/TranThiNha/myproject_quicknote