﻿// 1512364_QuickNote.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "1512364_QuickNote.h"

#include <windowsx.h>
#include <WinUser.h>
#include <wchar.h>
#include "Shobjidl.h"
#include "Shlobj.h"
#include <shellapi.h>
#include "TAG.h"
#include <ObjIdl.h>
#include <gdiplus.h>
#pragma comment(lib, "Gdiplus.lib")
using namespace Gdiplus;


#define WM_ME 1988
#define MAX_LOADSTRING 100
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

//Dùng để sử dụng hàm StrCpy, StrNCat
#include <shlwapi.h>
#pragma comment(lib, "shlwapi.lib")

// TreeView & ListView
#include <commctrl.h>
#pragma comment(lib, "Comctl32.lib")

// Global Variables:
HINSTANCE hInst;								// current instance
NOTIFYICONDATA niData;

// Forward declarations of functions included in this code module:

BOOL				InitInstance(HINSTANCE, int);
ULONGLONG			GetDllVersion(LPCTSTR lpszDllName);
LRESULT CALLBACK	AddNote(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	ViewNote(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	ViewChart(HWND, UINT, WPARAM, LPARAM);
void				ShowContextMenu(HWND hWnd);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
void				GetEachTag(WCHAR* x, vector<WCHAR*>&temp);
void				Load();

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MY1512364_QUICKNOTE));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}

HWND hWnd;
vector<TAG>listTag;
FILE *f;
FILE *rf;

GdiplusStartupInput gdiplusStartupInput;
ULONG_PTR           gdiplusToken;

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	InitCommonControls();
	hInst = hInstance;
	hWnd = CreateDialog(hInstance, MAKEINTRESOURCE(IDD_DIALOG),NULL,
		(DLGPROC)WndProc);
	if (!hWnd) return FALSE;

	ZeroMemory(&niData, sizeof(NOTIFYICONDATA));
	ULONGLONG ullVersion = GetDllVersion(_T("Shell32.dll"));

	if (ullVersion >= MAKEDLLVERULL(6, 0, 0, 0))
		niData.cbSize = sizeof(NOTIFYICONDATA);

	else if (ullVersion >= MAKEDLLVERULL(5, 0, 0, 0))
		niData.cbSize = NOTIFYICONDATA_V2_SIZE;

	else niData.cbSize = NOTIFYICONDATA_V1_SIZE;

	niData.uID = NULL;
	niData.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP;
	niData.hIcon = (HICON)LoadImage(hInstance,MAKEINTRESOURCE(IDI_NOTIFY),IMAGE_ICON,
		GetSystemMetrics(SM_CXSMICON),GetSystemMetrics(SM_CYSMICON),LR_DEFAULTCOLOR);

	niData.hWnd = hWnd;
	niData.uCallbackMessage = WM_ME;

	lstrcpyn(niData.szTip, _T("Quick Note"), sizeof(niData.szTip) / sizeof(TCHAR));

	Shell_NotifyIcon(NIM_ADD, &niData);

	// free icon handle
	if (niData.hIcon && DestroyIcon(niData.hIcon))
		niData.hIcon = NULL;

   return TRUE;
}

void ShowContextMenu(HWND hWnd)
{
	POINT pt;
	GetCursorPos(&pt);
	HMENU hMenu = CreatePopupMenu();
	if (hMenu)
	{
			InsertMenu(hMenu, -1, MF_BYPOSITION, WM_ME + 1, _T("Add note"));
			InsertMenu(hMenu, -1, MF_BYPOSITION, WM_ME + 3, _T("View notes"));
			InsertMenu(hMenu, -1, MF_BYPOSITION, WM_ME + 4, _T("View statitistics"));
			InsertMenu(hMenu, -1, MF_BYPOSITION, WM_ME + 5, _T("Exit"));
		// note:	must set window to the foreground or the
		//			menu won't disappear when it should
		SetForegroundWindow(hWnd);

		TrackPopupMenu(hMenu, TPM_BOTTOMALIGN,
			pt.x, pt.y, 0, hWnd, NULL);
		DestroyMenu(hMenu);
	}
}

int tong()
{
	int tong = 0;
	for (int i = 0; i < listTag.size(); i++)
	{
		tong += listTag[i].getSzListNote();
	}
	return tong;
}

float phanTram(TAG x)
{
	float pt= (float) x.getSzListNote() / tong();
	pt *= 100;
	return pt;
}


LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId = LOWORD(wParam);
	switch (message)
	{
	case WM_ME:
		switch (lParam)
		{
		case WM_RBUTTONDOWN:
			ShowContextMenu(hWnd);
			break;
		}
		break;
	case WM_COMMAND:
		switch (wmId)
		{
		case WM_ME + 1: //add note
			LoadLibrary(L"Shell32.dll");
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ADDNOTE), hWnd, (DLGPROC) AddNote);
			break;
		case WM_ME +3: //view note
			LoadLibrary(L"Shell32.dll");
			DialogBox(hInst, MAKEINTRESOURCE(IDD_VIEWNOTE), hWnd, (DLGPROC) ViewNote);
			break;
		case WM_ME +4:
			LoadLibrary(L"Shell32.dll");
			DialogBox(hInst, MAKEINTRESOURCE(IDD_VIEWCHART), hWnd, (DLGPROC)ViewChart);
			break;
		case WM_ME + 5: //exit
			PostQuitMessage(0);
			break;
		}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break; 
	}
	return 0;
}


HWND hStatic;
HWND hEditTag;
HWND hEditNote;
HWND hListTag;
HWND hListNote;
HWND hEContent;

LRESULT CALLBACK AddNote(HWND addNote, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	wmId = LOWORD(wParam);
	switch (message)
	{
	case WM_INITDIALOG:
	{
					f = _wfopen(L"Save.txt", L"ab");
					  INITCOMMONCONTROLSEX icc;
					  icc.dwSize = sizeof(icc);
					  icc.dwICC = ICC_WIN95_CLASSES;
					  InitCommonControlsEx(&icc);

					  LOGFONT lf;
					  GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
					  HFONT hFont = CreateFont(18, 7,
						  lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
						  lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
						  lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
						  lf.lfPitchAndFamily, TEXT("Segoe UI"));

					  hStatic = CreateWindowEx(0, L"STATIC", L"Tags:", WS_CHILD | WS_VISIBLE | SS_LEFT, 5, 8, 80, 20, addNote , NULL, hInst, NULL);
					  SendMessage(hStatic, WM_SETFONT, WPARAM(hFont), TRUE);
					  
					  hEditTag = CreateWindowEx(0, L"EDIT", L"", WS_CHILD | WS_VISIBLE | SS_LEFT|WS_BORDER|ES_AUTOHSCROLL, 5, 30, 402, 22, addNote, NULL, hInst, NULL);
					  SendMessage(hEditTag, WM_SETFONT, WPARAM(hFont), TRUE);

					  hStatic = CreateWindowEx(0, L"STATIC", L"Note:", WS_CHILD | WS_VISIBLE | SS_LEFT, 5, 57, 80, 20 , addNote, NULL, hInst, NULL);
					  SendMessage(hStatic, WM_SETFONT, WPARAM(hFont), TRUE);

					  hEditNote = CreateWindowEx(0, L"EDIT", L"", WS_CHILD | WS_VISIBLE | ES_LEFT | WS_BORDER | ES_MULTILINE | WS_HSCROLL | WS_VSCROLL|EN_SETFOCUS, 5, 80, 417, 400, addNote, NULL, hInst, NULL);
					  SendMessage(hEditNote, WM_SETFONT, WPARAM(hFont), TRUE);

					  break;
	}
	case WM_COMMAND:
		switch (wmId)
		{
		case BTN_SAVE:
		{
						 Load();
						 int sizeTag = GetWindowTextLength(hEditTag);
						 if (sizeTag == 0)
						 {
							 MessageBox(addNote, L"Missing tags",0,0);
							 break;
						 }
						 WCHAR* bufTag = new WCHAR[sizeTag + 1];
						 GetWindowText(hEditTag, bufTag, sizeTag+1);

						 int sizeNote = GetWindowTextLength(hEditNote);
						 if (sizeNote == 0)
						 {
							 MessageBox(addNote, L"Missing note", 0, 0);
							 break;
						 }
						 WCHAR* bufNote = new WCHAR[sizeNote + 1];
						 GetWindowText(hEditNote, bufNote, sizeNote+1);

						 vector<WCHAR*>tempTag; //lưu name tag nhập vào
						 GetEachTag(bufTag, tempTag);
						 
						 if (listTag.size() == 0)
						 {
							 TAG tempi;
							 tempi.setName(tempTag[tempTag.size()-1]);
							 tempi.addToList(bufNote);
							 listTag.push_back(tempi);
							 tempTag.pop_back();
						 }

						 if (tempTag.size() == 0) break;
						 int flag = 0;
						 for (int i = tempTag.size() - 1; i >= 0; i--)
						 {
							 for (int j = 0; j < listTag.size(); j++)
							 {
								 if (wcscmp(tempTag[i], listTag[j].getName())==0)  //nếu có tag: thêm note
								 {
									 listTag[j].addToList(bufNote);
									 flag = 1;
								 }
							 }
							 if (flag == 0)	//nếu chưa có: thêm tag, thêm note
							 {
								 TAG temp;
								 temp.setName(tempTag[i]);
								 temp.addToList(bufNote);
								 listTag.push_back(temp);
								 tempTag.pop_back();
							 }
							 
							 if (tempTag.size() == 0) break;
						 }
						 MessageBox(addNote, L"Save Successfully!", L"Quick Note",0);

						 SetWindowText(hEditTag, NULL);
						 SetWindowText(hEditNote, NULL);

						 break;
		}
		}
		break;
	case WM_CLOSE:
		//lưu file
		for (int a = 0; a < listTag.size(); a++)
		{
			fwrite(listTag[a].getName(), 2, wcslen(listTag[a].getName()), f);
			fwrite(L"*", 2, 1, f);
			vector<WCHAR*>Notes;
			listTag[a].getListNote(Notes);
			for (int b = 0; b < Notes.size(); b++)
			{
				fwrite(Notes[b], 2, wcslen(Notes[b]), f);
				fwrite(L"*", 2, 1, f);
			}
			fwrite(L"/", 2, 1, f);
		}
		fclose(f);
		listTag.clear();
		DestroyWindow(addNote);
		break;
	}
	return 0;
}

LRESULT CALLBACK ViewNote(HWND viewNote, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	wmId = LOWORD(wParam);
	switch (message)
	{
	case WM_INITDIALOG:
	{
						  Load();
						 
						  ListView_DeleteAllItems(hListTag);
						  LOGFONT lf;
						  GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
						  HFONT hFont = CreateFont(18, 7,
							  lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
							  lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
							  lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
							  lf.lfPitchAndFamily, TEXT("Segoe UI"));

						  hListTag = CreateWindowEx(NULL, WC_LISTVIEWW, L"",
							  WS_CHILD | WS_VISIBLE | WS_HSCROLL | WS_VSCROLL | ES_AUTOHSCROLL | ES_AUTOVSCROLL | LVS_REPORT | WS_BORDER | LVS_ALIGNTOP,
							  5, 7 , 150, 505, viewNote, (HMENU)IDC_LISTTAG, hInst, NULL);

						  hListNote = CreateWindowEx(NULL, WC_LISTVIEWW, L"",
							  WS_CHILD | WS_VISIBLE | WS_HSCROLL | WS_VSCROLL | ES_AUTOHSCROLL | ES_AUTOVSCROLL | LVS_REPORT | WS_BORDER | LVS_ALIGNTOP,
							 160, 7, 260, 505, viewNote, (HMENU)IDC_LISTNOTE, hInst, NULL);

						  hEContent = CreateWindowEx(0, L"EDIT", L"", WS_CHILD | WS_VISIBLE | SS_LEFT | WS_BORDER |ES_MULTILINE|WS_VSCROLL|WS_HSCROLL,
							 428, 7, 360, 505,viewNote, HMENU(IDC_CONTENT), hInst, NULL);
						  SendMessage(hEContent, WM_SETFONT, WPARAM(hFont), TRUE);
						 
						  LVCOLUMN lvCol;
						  lvCol.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT;
						  lvCol.fmt = LVCFMT_LEFT;
						  lvCol.cx = 150;
						  lvCol.pszText = L"TAGS:";
						  ListView_InsertColumn(hListTag, 0, &lvCol);
						 
						  lvCol.cx = 260;
						  lvCol.pszText = L"NOTES:";
						  ListView_InsertColumn(hListNote, 0, &lvCol);
						  

						  LVITEM lv;
						  lv.mask = LVIF_TEXT;
						  lv.iSubItem = 0;
						  for (int i = 0; i < listTag.size(); i++)
						  {
							  WCHAR* count = new WCHAR[20];
							  count = _itow(listTag[i].getSzListNote(), count, 10);
	
							  lv.pszText = wcscat(listTag[i].getName(),L" (");
							  lv.pszText = wcscat(listTag[i].getName(), count);
							  lv.pszText = wcscat(listTag[i].getName(), L")");
							  lv.iItem = i;
							  ListView_InsertItem(hListTag, &lv);
						  }
						  break;
	}
	case WM_NOTIFY:
	{
					 
					  switch (wmId)
					  {
					  case IDC_LISTTAG:
					  {
										  int index = ListView_GetNextItem(hListTag, -1, LVNI_FOCUSED);
										  LPNMHDR info = (LPNMHDR)lParam;
										  if (NM_CLICK == info->code)
										  {
											  ListView_DeleteAllItems(hListNote);
											  vector<WCHAR*>noteTemp;
											  listTag[index].getListNote(noteTemp);
											  LVITEM lv;
											  lv.mask = LVIF_TEXT | LVIF_PARAM;
											  lv.iSubItem = 0;
											  for (int i = 0; i < noteTemp.size(); i++)
											  {
												  lv.pszText = noteTemp[i];
												  lv.iItem = i;
												  lv.lParam = (LPARAM)index;
												  ListView_InsertItem(hListNote, &lv);
											  }
										  }
										  
									
										  break;
					  }
					  case IDC_LISTNOTE:
					  {
										   int i = ListView_GetNextItem(hListNote, -1, LVNI_FOCUSED);
										   int tag;

										   LVITEM lv;
										   lv.mask = LVIF_PARAM;
										   lv.iItem = i;
										   lv.iSubItem = 0;
										   ListView_GetItem(hListNote, &lv);

										   tag = lv.lParam;

										   LPNMHDR info = (LPNMHDR)lParam;
										   vector<WCHAR*>tNote;
										   if (NM_CLICK == info->code)
										   {
											   SetWindowText(hEContent, 0);
											   listTag[tag].getListNote(tNote);
											   SetWindowText(hEContent, tNote[i]);
										   }
										   break;
					  }
					  }
					  break;
	}
	case WM_CLOSE:
		listTag.clear();
		DestroyWindow(viewNote);
		break;
	}
	return 0;
}

LRESULT CALLBACK ViewChart(HWND viewChart, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	wmId = LOWORD(wParam);
	PAINTSTRUCT ps;
	HDC hdc;
	switch (message)
	{
	case WM_INITDIALOG:
	{
						  Load();
						  GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
						  break;
	}
	case WM_PAINT:
	{
					 hdc = BeginPaint(viewChart, &ps);

					 Graphics graphics(hdc);

					 SolidBrush brush1(Color(128, 128, 255));
					 SolidBrush brush2(Color(255, 128, 128));
					 SolidBrush brush3(Color(0, 255, 64));
					 SolidBrush brush4(Color(128, 0, 255));
					 SolidBrush brush5(Color(255, 128, 192));
					 SolidBrush brush6(Color(255, 128, 64));
					 SolidBrush brushnull(Color(192, 192, 192));
					 SolidBrush* mangmau[6] = { &brush1, &brush2, &brush3, &brush4, &brush5, &brush6 };


					 LOGFONT lf;
					 GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
					 HFONT hFont = CreateFont(18, 7,
						 lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
						 lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
						 lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
						 lf.lfPitchAndFamily, TEXT("Segoe UI"));

					 //Vẽ biểu đồ
					 int flag = 0;
					 float use = 0;
					 int vt = 25;
					 for (int i = 0; i < listTag.size(); i++)
					 {
						 if (phanTram(listTag[i]) != 0)
						 {
							 graphics.FillPie(mangmau[i], Rect(80, 60, 250, 250), use, phanTram(listTag[i])*3.6);
							 use += phanTram(listTag[i])*3.6;
							 graphics.FillPie(mangmau[i], Rect(380, vt, 100, 100), 0, 30);
							
							 HWND hTemp = CreateWindowEx(0, L"STATIC", listTag[i].getName(), WS_CHILD | WS_VISIBLE | SS_LEFT, 500, vt+50, 50, 20, viewChart, NULL, hInst, NULL);
							 SendMessage(hTemp, WM_SETFONT, WPARAM(hFont), TRUE);
							 
							 HWND hPt =  CreateWindowEx(0, L"EDIT", L"", WS_CHILD | WS_VISIBLE | SS_LEFT | ES_AUTOHSCROLL|ES_READONLY, 550, vt+48, 50, 20, viewChart, NULL, hInst, NULL);
							 SendMessage(hPt, WM_SETFONT, WPARAM(hFont), TRUE);
							 WCHAR* ptTemp = new WCHAR[4];
							 ptTemp = _itow(phanTram(listTag[i]), ptTemp, 10);
							 wcscat(ptTemp, L"%");
							 SetWindowText(hPt, ptTemp);

							 flag = 1;
							 vt += 60;
						 }
					 }

					 HWND hTemp = CreateWindowEx(0, L"STATIC", L"Chart based on tags and number of corresponding notes", WS_CHILD | WS_VISIBLE | SS_LEFT, 50, 350, 350, 25, viewChart, NULL, hInst, NULL);
					 SendMessage(hTemp, WM_SETFONT, WPARAM(hFont), TRUE);

					 if (flag == 0)  graphics.FillPie(&brushnull, Rect(80, 60, 250, 250), 90, 360);



					 // TODO: Add any drawing code here...
					 EndPaint(hWnd, &ps);
					 break;
	}
	case WM_CLOSE:
		GdiplusShutdown(gdiplusToken);
		listTag.clear();
		DestroyWindow(viewChart);
		break;
	}
	return 0;
}

ULONGLONG GetDllVersion(LPCTSTR lpszDllName)
{
	ULONGLONG ullVersion = 0;
	HINSTANCE hinstDll;
	hinstDll = LoadLibrary(lpszDllName);
	if (hinstDll)
	{
		DLLGETVERSIONPROC pDllGetVersion;
		pDllGetVersion = (DLLGETVERSIONPROC)GetProcAddress(hinstDll, "DllGetVersion");
		if (pDllGetVersion)
		{
			DLLVERSIONINFO dvi;
			HRESULT hr;
			ZeroMemory(&dvi, sizeof(dvi));
			dvi.cbSize = sizeof(dvi);
			hr = (*pDllGetVersion)(&dvi);
			if (SUCCEEDED(hr))
				ullVersion = MAKEDLLVERULL(dvi.dwMajorVersion, dvi.dwMinorVersion, 0, 0);
		}
		FreeLibrary(hinstDll);
	}
	return ullVersion;
}

//truyền chuỗi dạng: abc, xyz
void GetEachTag(WCHAR* x, vector<WCHAR*>&temp)
{
	WCHAR* tag;

	tag = wcstok(x, L", ");
	while (tag != NULL)
	{
		temp.push_back(tag);
		tag = wcstok(NULL, L", ");
	}
}

void Load()
{
	rf = _wfopen(L"Save.txt", L"rb");
	if (rf == NULL) return;
	WCHAR* buffer;
	int i = 0;
	while (!feof(rf))
	{
		TAG temp;
		buffer = new WCHAR[1000];
		i = 0;
		while (!feof(rf))
		{
			fread(&buffer[i], 2, 1, rf);
			if (buffer[i] == 0) break;
			if (buffer[i] == L'*')
			{
				buffer[i] = 0;
				temp.setName(buffer);
				break;
			}
			i++;
		}

		buffer = new WCHAR[1000];
		i = 0;
		while (!feof(rf))
		{
			fread(&buffer[i], 2, 1, rf);
			if (buffer[i] == 0) break;
			if (buffer[i] == L'*')
			{
				buffer[i] = 0;
				temp.addToList(buffer);
				i = -1;
			}
			if (buffer[i] == L'/')
			{
				break;
			}
			i++;
		}
		i++;
		if (temp.getName() != 0 && temp.getSzListNote() != 0)
		{
			listTag.push_back(temp);
		}
	}
	fclose(rf);
}