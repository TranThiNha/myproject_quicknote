#pragma once
#include <string>
#include <vector>
#include "NOTE.h"
using namespace std;

class TAG
{
private:
	WCHAR* name;
	vector<NOTE*>listNote;
public:
	void setName(WCHAR* x)
	{ 
		name = new WCHAR[1000];
		wcscpy(name, x);
	};
	WCHAR* getName(){ return name; };
	void addToList(WCHAR* x)
	{
		NOTE* temp = new NOTE;
		temp->setContent(x);
		listNote.push_back(temp);
	};
	void getListNote(vector<WCHAR*>&Note)
	{
		for (int i = 0; i < listNote.size(); i++)
		{
			Note.push_back(listNote[i]->getContent());
		}
	};
	int getSzListNote(){ return listNote.size(); };
	TAG();
	~TAG();
};

